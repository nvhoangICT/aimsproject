import hust.soict.ictglobal.aims.media.DigitalVideoDisc;

public class TestPassingParameter {

	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle", "Animation", 19.95f, "Roger Allers", 87);
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella", "Animation", 16.99f, "Clyde Geronimi", 74);

		System.out.println("BEFORE SWAP:");
		System.out.println("jungleDVD: " + jungleDVD.getTitle());
		System.out.println("cinderellaDVD: " + cinderellaDVD.getTitle());

		swap(jungleDVD, cinderellaDVD);

		System.out.println("\nAFTER SWAP:");
		System.out.println("jungleDVD: " + jungleDVD.getTitle());
		System.out.println("cinderellaDVD: " + cinderellaDVD.getTitle());

		changeTitle(jungleDVD, cinderellaDVD.getTitle());

		System.out.println("\nAFTER changeTitle:");
		System.out.println("jungleDVD: " + jungleDVD.getTitle());
		System.out.println("cinderellaDVD: " + cinderellaDVD.getTitle());
	}

	public static void swap(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc temp = dvd1;
		dvd1 = dvd2;
		dvd2 = temp;
	}

	public static void changeTitle(DigitalVideoDisc dvd, String newTitle) {
//		String oldTitle = dvd.getTitle();
		dvd.setTitle(newTitle);
//		dvd = new DigitalVideoDisc(oldTitle);
	}
}
