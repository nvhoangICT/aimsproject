import hust.soict.ictglobal.date.MyDate;

public class DateTest {
	public static void main(String[] args) {
		// Kiểm tra các phương thức của lớp MyDate với các tình huống khác nhau

		// Tạo một đối tượng MyDate bằng constructor mặc định
		MyDate date1 = new MyDate();
		System.out.print("Current Date: ");
		date1.print();
		date1.print("dd/mm/yyyy");
		date1.print("yy-mm-dd");
		date1.print("Today is the dd day of mm in yyyy");

		// Tạo một đối tượng MyDate bằng constructor với tham số ngày, tháng, năm
		MyDate date2 = new MyDate(18, 2, 2019);
		System.out.print("Date from parameters: ");
		date2.print();

		// Tạo một đối tượng MyDate bằng constructor với tham số kiểu String
		MyDate date3 = new MyDate("February 8 2019");
		System.out.print("Date from String: ");
		date3.print();

		// Tạo một đối tượng MyDate và nhập từ bàn phím
		MyDate date4 = new MyDate();
		date4.accept();
		System.out.print("Date entered by user: ");
		date4.print();
	}
}
