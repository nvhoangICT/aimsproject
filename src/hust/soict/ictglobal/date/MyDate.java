package hust.soict.ictglobal.date;
import java.util.Calendar;
import java.util.Scanner;

public class MyDate {

	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public MyDate() {
		// Lấy thời gian hiện tại
		Calendar calendar = Calendar.getInstance();

		// Gán giá trị ngày, tháng và năm từ thời gian hiện tại
		this.day = calendar.get(Calendar.DAY_OF_MONTH);
		this.month = calendar.get(Calendar.MONTH) + 1; // Tháng bắt đầu từ 0
		this.year = calendar.get(Calendar.YEAR);
	}

	public MyDate(String date) {
		// Tách chuỗi date thành các thành phần: month, day, year
		String[] dateParts = date.split(" ");
		if (dateParts.length == 3) {
			String monthStr = dateParts[0];
			String dayStr = dateParts[1];
			String yearStr = dateParts[2];

			// Chuyển đổi tháng thành số
			switch (monthStr.toLowerCase()) {
			case "january":
				month = 1;
				break;
			case "february":
				month = 2;
				break;
			case "march":
				month = 3;
				break;
			case "april":
				month = 4;
				break;
			case "may":
				month = 5;
				break;
			case "june":
				month = 6;
				break;
			case "july":
				month = 7;
				break;
			case "august":
				month = 8;
				break;
			case "september":
				month = 9;
				break;
			case "october":
				month = 10;
				break;
			case "november":
				month = 11;
				break;
			case "december":
				month = 12;
				break;
			default:
				System.out.println("Invalid month format.");
				break;
			}

			// Chuyển đổi ngày và năm thành số
			try {
				day = Integer.parseInt(dayStr);
				year = Integer.parseInt(yearStr);
			} catch (NumberFormatException e) {
				System.out.println("Invalid day or year format.");
			}
		} else {
			System.out.println(
					"Invalid date format. Please enter date in format 'Month day year', e.g., 'February 18 2019'.");
		}
	}

	private int day;
	private int month;
	private int year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setDay(String dayString) {
		// Chuyển đổi chuỗi thành giá trị nguyên và gán cho day
		switch (dayString.toLowerCase()) {
		case "first":
			this.day = 1;
			break;
		case "second":
			this.day = 2;
			break;
		case "third":
			this.day = 3;
			break;
		case "fourth":
			this.day = 4;
			break;
		case "fifth":
			this.day = 5;
			break;
		case "sixth":
			this.day = 6;
			break;
		case "seventh":
			this.day = 7;
			break;
		case "eighth":
			this.day = 8;
			break;
		case "ninth":
			this.day = 9;
			break;
		case "tenth":
			this.day = 10;
			break;
		case "eleventh":
			this.day = 11;
			break;
		case "twelfth":
			this.day = 12;
			break;
		case "thirteenth":
			this.day = 13;
			break;
		case "fourteenth":
			this.day = 14;
			break;
		case "fifteenth":
			this.day = 15;
			break;
		case "sixteenth":
			this.day = 16;
			break;
		case "seventeenth":
			this.day = 17;
			break;
		case "eighteenth":
			this.day = 18;
			break;
		case "nineteenth":
			this.day = 19;
			break;
		case "twentieth":
			this.day = 20;
			break;
		case "twenty-first":
			this.day = 21;
			break;
		case "twenty-second":
			this.day = 22;
			break;
		case "twenty-third":
			this.day = 23;
			break;
		case "twenty-fourth":
			this.day = 24;
			break;
		case "twenty-fifth":
			this.day = 25;
			break;
		case "twenty-sixth":
			this.day = 26;
			break;
		case "twenty-seventh":
			this.day = 27;
			break;
		case "twenty-eighth":
			this.day = 28;
			break;
		case "twenty-ninth":
			this.day = 29;
			break;
		case "thirtieth":
			this.day = 30;
			break;
		case "thirty-first":
			this.day = 31;
			break;
		default:
			// Xử lý trường hợp không hợp lệ, có thể hiển thị thông báo hoặc gán giá trị mặc
			// định
			System.out.println("Invalid day value: " + dayString);
			break;
		}
	}

	public void setMonth(String monthString) {
		// Chuyển đổi chuỗi thành giá trị nguyên và gán cho month
		switch (monthString.toLowerCase()) {
		case "january":
			this.month = 1;
			break;
		case "february":
			this.month = 2;
			break;
		case "march":
			this.month = 3;
			break;
		case "april":
			this.month = 4;
			break;
		case "may":
			this.month = 5;
			break;
		case "june":
			this.month = 6;
			break;
		case "july":
			this.month = 7;
			break;
		case "august":
			this.month = 8;
			break;
		case "september":
			this.month = 9;
			break;
		case "october":
			this.month = 10;
			break;
		case "november":
			this.month = 11;
			break;
		case "december":
			this.month = 12;
			break;
		default:
			// Xử lý trường hợp không hợp lệ, có thể hiển thị thông báo hoặc gán giá trị mặc
			// định
			System.out.println("Invalid month value: " + monthString);
			break;
		}
	}

	public void setYear(String yearString) {
		// Chuyển đổi chuỗi thành giá trị nguyên và gán cho year
		switch (yearString.toLowerCase()) {
		case "twenty twenty":
			this.year = 2020;
			break;
		case "twenty twenty-one":
			this.year = 2021;
			break;
		case "twenty twenty-two":
			this.year = 2022;
			break;
		case "twenty twenty-three":
			this.year = 2023;
			break;
		default:
			// Xử lý trường hợp không hợp lệ, có thể hiển thị thông báo hoặc gán giá trị mặc
			// định
			System.out.println("Invalid year value: " + yearString);
			break;
		}
	}

	public void accept() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a date (e.g., 'February 18 2019'): ");
		String inputDate = scanner.nextLine();
		scanner.close();

		// Tương tự phần xử lý chuỗi ở constructor với tham số kiểu String
		String[] dateParts = inputDate.split(" ");
		if (dateParts.length == 3) {
			String monthStr = dateParts[0];
			String dayStr = dateParts[1];
			String yearStr = dateParts[2];

			// Chuyển đổi tháng thành số
			switch (monthStr.toLowerCase()) {
			case "january":
				month = 1;
				break;
			case "february":
				month = 2;
				break;
			case "march":
				month = 3;
				break;
			case "april":
				month = 4;
				break;
			case "may":
				month = 5;
				break;
			case "june":
				month = 6;
				break;
			case "july":
				month = 7;
				break;
			case "august":
				month = 8;
				break;
			case "september":
				month = 9;
				break;
			case "october":
				month = 10;
				break;
			case "november":
				month = 11;
				break;
			case "december":
				month = 12;
				break;
			default:
				System.out.println("Invalid month format.");
				break;
			}

			// Chuyển đổi ngày và năm thành số
			try {
				day = Integer.parseInt(dayStr);
				year = Integer.parseInt(yearStr);
			} catch (NumberFormatException e) {
				System.out.println("Invalid day or year format.");
			}
		} else {
			System.out.println(
					"Invalid date format. Please enter date in format 'Month day year', e.g., 'February 18 2019'.");
		}
	}

	public void print() {
		String monthStr = ""; // Biến lưu tên tháng

		// Chuyển đổi số tháng thành tên tháng
		switch (month) {
		case 1:
			monthStr = "January";
			break;
		case 2:
			monthStr = "February";
			break;
		case 3:
			monthStr = "March";
			break;
		case 4:
			monthStr = "April";
			break;
		case 5:
			monthStr = "May";
			break;
		case 6:
			monthStr = "June";
			break;
		case 7:
			monthStr = "July";
			break;
		case 8:
			monthStr = "August";
			break;
		case 9:
			monthStr = "September";
			break;
		case 10:
			monthStr = "October";
			break;
		case 11:
			monthStr = "November";
			break;
		case 12:
			monthStr = "December";
			break;
		default:
			monthStr = "Invalid Month";
			break;
		}

		// In ngày ra màn hình
		System.out.println(monthStr + " " + day + " " + year);
	}

	public void print(String format) {
		String formattedDate = format; // Biến lưu trữ kết quả định dạng

		// Thay thế các giá trị định dạng với ngày, tháng, năm thực tế
		formattedDate = formattedDate.replace("dd", String.format("%02d", day)); // Ngày
		formattedDate = formattedDate.replace("mm", String.format("%02d", month)); // Tháng
		formattedDate = formattedDate.replace("yyyy", String.format("%04d", year)); // Năm
		formattedDate = formattedDate.replace("yy", String.format("%02d", year % 100)); // Năm (2 chữ số)

		// In ngày đã định dạng ra màn hình
		System.out.println(formattedDate);
	}

}
