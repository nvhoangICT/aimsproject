package hust.soict.ictglobal.aims.order;

import java.util.ArrayList;
import java.util.List;

import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.date.MyDate;

public class Order {
	private static final int MAX_NUM_ITEMS = 10;
	private int qtyOrdered = 0;
	private MyDate dateOrdered;
	private static int nbOrders = 0;
	private static final int MAX_LIMITTED_ORDERS = 10;
	private List<Media> itemsOrdered = new ArrayList<Media>();

	public Order() {
		// Khởi tạo biến dateOrdered với thời gian hiện tại
		dateOrdered = new MyDate();
		// Cập nhật số lượng đơn hàng đã tạo và kiểm tra giới hạn
		nbOrders++;
		if (nbOrders > MAX_LIMITTED_ORDERS) {
			System.out.println("Cannot create a new order. Maximum orders reached.");
		}
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public void addMedia(Media media) {
		if (media != null && itemsOrdered.size() < MAX_NUM_ITEMS) {
			itemsOrdered.add(media);
			System.out.println("The media " + media.getTitle() + " has been added.");
		} else {
			System.out.println("The order is full or the media is null. Cannot add more media.");
		}
	}

	public void removeMedia(Media media) {
		if (media != null) {
			if (itemsOrdered.remove(media)) {
				System.out.println("The media has been removed.");
			} else {
				System.out.println("The media was not found in the order.");
			}
		}
	}

	public float totalCost() {
		float total = 0.0f;
		for (Media media : itemsOrdered) {
			total += media.getCost();
		}
		return total;
	}

	// Thêm phương thức để in thông tin đơn hàng
	public void printOrder() {
		System.out.println("*********************Order**************** ********");
		dateOrdered.print("dd/mm/yyyy");
		System.out.println("Ordered Items:");

		for (int i = 0; i < itemsOrdered.size(); i++) {
			Media media = itemsOrdered.get(i);
			System.out.println((i + 1) + ". " + media.toString());
		}

		float totalCost = totalCost();
		System.out.println("Total cost: " + totalCost);
		System.out.println("**************************************************");
	}

	public Media findMediaByTitle(String title) {
		for (Media media : itemsOrdered) {
			if (media.getTitle().equalsIgnoreCase(title)) {
				return media;
			}
		}
		return null;
	}

}
