package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {

	public static void main(String[] args) {
		// Tạo và bắt đầu luồng daemon
//		MemoryDaemon memoryDaemon = new MemoryDaemon();
//		Thread daemonThread = new Thread(memoryDaemon);
//		daemonThread.setDaemon(true); // Đánh dấu luồng là luồng daemon
//		daemonThread.start();

		List<Media> mediaList = new ArrayList<>();

		// Thêm các đối tượng Media vào danh sách
		CompactDisc cd1 = new CompactDisc("CD 3", "Music", 9.99f, 60, "Artist 1");
		CompactDisc cd2 = new CompactDisc("CD 1", "Music", 7.99f, 45, "Artist 2");
		CompactDisc cd3 = new CompactDisc("CD 2", "Music", 6.99f, 55, "Artist 3");

		mediaList.add(cd1);
		mediaList.add(cd2);
		mediaList.add(cd3);

		Collections.sort(mediaList);

		// Hiển thị danh sách sau khi đã sắp xếp
		System.out.println("\nDanh sách sau khi sắp xếp:");
		for (Media media : mediaList) {
			System.out.println(media);
		}

		// Chương trình chính
		Scanner scanner = new Scanner(System.in);
		Order order = null;
		boolean exit = false;
		String playChoice;

		while (!exit) {
			showMenu();
			int choice = scanner.nextInt();
			scanner.nextLine(); // Đọc ký tự Enter sau lựa chọn số

			switch (choice) {
			case 1:
				// Tạo một đơn hàng mới
				order = new Order();
				System.out.println("A new order has been created.");
				break;
			case 2:
				// Thêm một mục vào đơn hàng
				if (order == null) {
					System.out.println("Please create an order first.");
				} else {
					System.out.println("Choose item type to add (1. Book, 2. CompactDisc, 3. DigitalVideoDisc): ");
					int itemType = scanner.nextInt();
					scanner.nextLine(); // Đọc dòng trống sau khi nhập lựa chọn loại sản phẩm

					switch (itemType) {
					case 1:
						// Thêm sách (Book)
						System.out.println("Enter Book information:");
						System.out.print("Title: ");
						String bookTitle = scanner.nextLine();
						System.out.print("Category: ");
						String bookCategory = scanner.nextLine();
						System.out.print("Cost: ");
						float bookCost = scanner.nextFloat();
						scanner.nextLine(); // Đọc dòng trống

						ArrayList<String> authors = new ArrayList<String>();
						System.out.print("Enter the number of authors: ");
						int numAuthors = scanner.nextInt();
						scanner.nextLine(); // Đọc dòng trống
						for (int i = 0; i < numAuthors; i++) {
							System.out.print("Author " + (i + 1) + ": ");
							String author = scanner.nextLine();
							authors.add(author);
						}
						System.out.print("Content: ");
						String bookContent = scanner.nextLine();
						Book book = new Book(bookTitle, bookCategory, bookCost, authors, bookContent);
						order.addMedia(book);
						break;
					case 2:
						// Thêm đĩa CD (CompactDisc)
						System.out.println("Enter CompactDisc information:");
						System.out.print("Title: ");
						String cdTitle = scanner.nextLine();
						System.out.print("Category: ");
						String cdCategory = scanner.nextLine();
						System.out.print("Cost: ");
						float cdCost = scanner.nextFloat();
						scanner.nextLine(); // Đọc dòng trống

						// Nhập thông tin về độ dài và đạo diễn
						System.out.print("Length: ");
						int cdLength = scanner.nextInt();
						scanner.nextLine(); // Đọc dòng trống
						System.out.print("Director: ");
						String cdDirector = scanner.nextLine();

						CompactDisc cd = new CompactDisc(cdTitle, cdCategory, cdCost, cdLength, cdDirector);

						System.out.print("Enter the number of tracks: ");
						int numTracks = scanner.nextInt();
						scanner.nextLine(); // Đọc dòng trống
						for (int i = 0; i < numTracks; i++) {
							System.out.println("Enter Track " + (i + 1) + " information:");
							System.out.print("Title: ");
							String trackTitle = scanner.nextLine();
							System.out.print("Length: ");
							int trackLength = scanner.nextInt();
							scanner.nextLine(); // Đọc dòng trống

							// Sử dụng constructor mới của Track
							Track track = new Track(trackTitle, trackLength);
							cd.addTrack(track);
						}
						order.addMedia(cd);

						// Hỏi người dùng có muốn chơi đĩa CD không
						System.out.print("Do you want to play this CompactDisc? (Y/N): ");
						playChoice = scanner.nextLine();

						if (playChoice.equalsIgnoreCase("Y")) {
							try {
								cd.play();
							} catch (PlayerException e) {
								System.err.println("PlayerException occurred: " + e.getMessage());
								e.printStackTrace();
							}
						}
						break;
					case 3:
						// Thêm đĩa DVD (DigitalVideoDisc)
						System.out.println("Enter DigitalVideoDisc information:");
						System.out.print("Title: ");
						String dvdTitle = scanner.nextLine();
						System.out.print("Category: ");
						String dvdCategory = scanner.nextLine();
						System.out.print("Cost: ");
						float dvdCost = scanner.nextFloat();
						scanner.nextLine(); // Đọc dòng trống

						// Nhập thông tin về độ dài và đạo diễn
						System.out.print("Length: ");
						int dvdLength = scanner.nextInt();
						scanner.nextLine(); // Đọc dòng trống
						System.out.print("Director: ");
						String dvdDirector = scanner.nextLine();

						// Sử dụng constructor mới của DigitalVideoDisc
						DigitalVideoDisc dvd = new DigitalVideoDisc(dvdTitle, dvdCategory, dvdCost, dvdLength,
								dvdDirector);
						order.addMedia(dvd);

						// Hỏi người dùng có muốn chơi đĩa DVD không
						System.out.print("Do you want to play this DigitalVideoDisc? (Y/N): ");
						playChoice = scanner.nextLine();

						if (playChoice.equalsIgnoreCase("Y")) {
							try {
								dvd.play();
							} catch (PlayerException e) {
								System.err.println("PlayerException occurred: " + e.getMessage());
								e.printStackTrace();
							}
						}
						break;
					default:
						System.out.println("Invalid item type.");
						break;
					}
				}
				break;
			case 3:
				// Xóa một mục khỏi đơn hàng
				if (order != null) {
					System.out.print("Enter media title to delete: ");
					String titleToDelete = scanner.nextLine();

					// Tìm một Media có title cụ thể trong đơn hàng và xóa nó
					Media mediaToDelete = order.findMediaByTitle(titleToDelete);
					if (mediaToDelete != null) {
						order.removeMedia(mediaToDelete);
						System.out.println("The media has been removed from the order.");
					} else {
						System.out.println("The media was not found in the order.");
					}
				} else {
					System.out.println("Please create a new order first.");
				}
				break;
			case 4:
				// Hiển thị danh sách các mục trong đơn hàng
				if (order != null) {
					System.out.println("Items in the order:");
					order.printOrder();
				} else {
					System.out.println("Please create a new order first.");
				}
				break;
			case 0:
				// Thoát khỏi ứng dụng
				exit = true;
				break;
			default:
				System.out.println("Invalid choice. Please choose a valid option.");
				break;
			}
		}

		scanner.close();
	}

	public static void showMenu() {
		System.out.println("Order Management Application:");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}

}
