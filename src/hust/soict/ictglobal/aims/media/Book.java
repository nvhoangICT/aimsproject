package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media implements Comparable<Media> {

	public Book(String title, String category, float cost, List<String> authors, String content) {
		super(title, category, cost);
		this.authors = authors;
		this.content = content;
		this.contentTokens = new ArrayList<>();
		this.wordFrequency = new TreeMap<>();
		processContent();
	}

	private List<String> authors = new ArrayList<>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<String> authors) {
		this.authors = authors;
	}

	public void addAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			authors.add(authorName);
		}
	}

	public void removeAuthor(String authorName) {
		if (authors.contains(authorName)) {
			authors.remove(authorName);
		}
	}

	@Override
	public int compareTo(Media o) {
		return this.getTitle().compareTo(o.getTitle());
	}

	private void processContent() {
		String[] tokens = content.split("[\\s\\p{Punct}]+");
		Arrays.sort(tokens);

		contentTokens.addAll(Arrays.asList(tokens));

		for (String token : contentTokens) {
			wordFrequency.put(token, wordFrequency.getOrDefault(token, 0) + 1);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Title: ").append(title).append("\n");
		sb.append("Content Length: ").append(contentTokens.size()).append(" tokens\n");
		sb.append("Content Tokens: ").append(contentTokens).append("\n");
		sb.append("Word Frequency:\n");
		for (Map.Entry<String, Integer> entry : wordFrequency.entrySet()) {
			sb.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
		}
		return sb.toString();
	}

}
