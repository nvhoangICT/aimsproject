package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class Track implements Playable, Comparable<Track> {

	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}

	private String title;
	private int length;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public void play() throws PlayerException {
		if (getLength() <= 0) {
			System.err.println("Error: This Track cannot be played due to invalid length.");
			throw new PlayerException();
		}
		// Tiếp tục phát track
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(Track o) {
		return this.getTitle().compareTo(o.getTitle());
	}

}
