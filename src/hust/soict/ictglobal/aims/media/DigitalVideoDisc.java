package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Media> {

	public DigitalVideoDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
	}

	private String director;
	private int length;

	@Override
	public String getDirector() {
		return director;
	}

	@Override
	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public int getLength() {
		return length;
	}

	@Override
	public void setLength(int length) {
		this.length = length;
	}

	public boolean search(String title) {
		return this.getTitle().toLowerCase().contains(title.toLowerCase());
	}

	@Override
	public void play() throws PlayerException {
		if (getLength() <= 0) {
			System.err.println("Error: This DigitalVideoDisc cannot be played due to invalid length.");
			throw new PlayerException();
		}
		// Tiếp tục phát đĩa
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(Media o) {
		// So sánh giá của DVD hiện tại và DVD khác
		if (this.getCost() < o.getCost()) {
			return -1; // Trả về -1 nếu giá của DVD hiện tại nhỏ hơn
		} else if (this.getCost() > o.getCost()) {
			return 1; // Trả về 1 nếu giá của DVD hiện tại lớn hơn
		} else {
			return 0; // Trả về 0 nếu giá của cả hai bằng nhau
		}
	}

}
