package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public interface Playable {
	abstract void play() throws PlayerException;
}
