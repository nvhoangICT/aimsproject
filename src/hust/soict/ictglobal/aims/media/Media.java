package hust.soict.ictglobal.aims.media;

import java.util.Objects;

public abstract class Media implements Comparable<Media> {

	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	public Media(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}

	public Media(String title) {
		super();
		this.title = title;
	}

	protected String title;
	private String category;
	private float cost;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@Override
	public String toString() {
		return "Title: " + title + ", Category: " + category + ", Cost: " + cost + " $";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		Media otherMedia;
		try {
			otherMedia = (Media) obj;
		} catch (ClassCastException e) {
			return false;
		}

		return Float.compare(otherMedia.cost, cost) == 0 && Objects.equals(title, otherMedia.title);
	}

	@Override
	public int compareTo(Media otherMedia) {
		if (this == otherMedia) {
			return 0;
		}

		if (otherMedia == null) {
			throw new NullPointerException("The argument 'otherMedia' is null");
		}

		if (!(otherMedia instanceof Media)) {
			throw new ClassCastException("Cannot compare to an object of a different class");
		}

		int titleComparison = title.compareTo(otherMedia.title);
		if (titleComparison != 0) {
			return titleComparison;
		}

		return Float.compare(cost, otherMedia.cost);
	}

}
