package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

import hust.soict.ictglobal.aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable<Media> {

	public CompactDisc(String title, String category, float cost, int length, String director) {
		super(title, category, cost, length, director);
	}

	private String artist;
	private ArrayList<Track> tracks = new ArrayList<>();

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void addTrack(Track track) {
		if (track != null) {
			if (tracks.contains(track)) {
				System.out.println("Track " + track.getTitle() + " is already in the list of tracks.");
			} else {
				tracks.add(track);
				System.out.println("Track " + track.getTitle() + " has been added to the list of tracks.");
			}
		}
	}

	public void removeTrack(Track track) {
		if (track != null) {
			if (tracks.contains(track)) {
				tracks.remove(track);
				System.out.println("Track " + track.getTitle() + " has been removed from the list of tracks.");
			} else {
				System.out.println("Track " + track.getTitle() + " is not in the list of tracks.");
			}
		}
	}

	@Override
	public int getLength() {
		int totalLength = 0;
		for (Track track : tracks) {
			totalLength += track.getLength();
		}
		return totalLength;
	}

	@Override
	public void play() throws PlayerException {
		System.out.println("Playing CD: " + this.getTitle() + " by " + this.getArtist());
		System.out.println("Total CD length: " + this.getLength() + " minutes");
		System.out.println("Tracks:");
		if (getLength() <= 0) {
			System.err.println("Error: This CompactDisc cannot be played due to invalid length.");
			throw new PlayerException("Invalid Media Length");
		}

		// Tiếp tục phát đĩa CD và xử lý ngoại lệ từ các track
		for (Track track : tracks) {
			try {
				track.play();
			} catch (PlayerException e) {
				System.err.println("Error while playing track: " + e.getMessage());
			}
		}
	}

	@Override
	public int compareTo(Media o) {
		return this.getTitle().compareTo(o.getTitle());
	}

}
