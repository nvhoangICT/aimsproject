package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class BookTest {

	public static void main(String[] args) {
		String content = "This is a sample book. This book has sample content.";
		List<String> authors = new ArrayList<>();
		authors.add("John Doe");
		Book book = new Book("Sample Book", "Fiction", 19.99f, authors, content);

		System.out.println(book);
	}
}
