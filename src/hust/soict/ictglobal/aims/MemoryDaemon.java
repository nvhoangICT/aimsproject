package hust.soict.ictglobal.aims;

public class MemoryDaemon implements Runnable {
	private long memoryUsed = 0;
	Runtime runtime = Runtime.getRuntime();

	@Override
	public void run() {
		while (true) {
			long totalMemory = runtime.totalMemory();
			long freeMemory = runtime.freeMemory();
			memoryUsed = totalMemory - freeMemory;

			// Log thông tin sử dụng bộ nhớ
			System.out.println("Memory Used: " + memoryUsed + " bytes");

			try {
				// Ngủ 1 giây trước khi kiểm tra lại
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
